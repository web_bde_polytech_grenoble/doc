#### Start Page
[Go to start page](./README.md)

# Techno

Front : [source code](https://gitlab.com/web_bde_polytech_grenoble/front_web_bde)
- AngularCLI (TypeScript, JS, HTML, CSS/SCSS)
- PWA : Progressiv Web app (To use the WebSite like a native application on smartphone -> open the web site with your favorite browser, go on the parameters and click on "Add to start screen")

Back : 
- app : [source code](https://gitlab.com/web_bde_polytech_grenoble/ade_scrapper)
    - python : Allow to get data from Polytech ADE Serveur because this server is close at night. 

    /!\ will change soon

    - Firebase : Allow to save the data getting by the python application. 

    /!\ will change soon

- API rest : [source code](https://gitlab.com/web_bde_polytech_grenoble/api-bde)
     
# Final Specification
## Private Pages
Page with restricted access by login request.

#### News
Page where all facebook publications of the BDE appear for people who don't have facebook account.

#### Shortcut
Page with shortcut depending to the field of the student containing links he needs 

exemple : moodle, Chamilo, léo

#### Agenda
https://edt.bdepolytechgrenoble.fr -> integrate this, with automatique parameters using info of user

https://gitlab.com/noade-calendar -> source code

## Public Pages
Page accessible to all without connection request.

#### Home

#### Branchs 
Page with presentation of all field of the school (INFo, PRI, ...)

https://www.notion.so/Accueil-PGNN-60e27d15664146e892535599d3cbb7ec

#### Association
Page with the list of association of the school like BDE, BDA, ... and all of subcellular (with social network :

#### Contact

#### Discount Cards

#### Events

#### Partnerships

#### Songs

#### The city