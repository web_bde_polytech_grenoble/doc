#### Start Page
[Go to start page](./README.md)

# Conventions

## Structure 

The struture used is Component Structure : we separate folders component, services, shared.
Folder Component contain all component folder.
Folder Services contain all service file.
Folder Shared contain all component folder and service file used into some component and can be used simply in an other project.

## Syntax
- table : 
    * in ts file (variable used on html) : 
    _myTab
    * other case : 
    myTab
- other varaible : 
    myVar
- function : 
    myFunction
- variable into enum : 
    WithUpperCase

