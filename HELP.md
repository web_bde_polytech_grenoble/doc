#### Start Page
[Go to start page](./README.md)

# Start with project
Download the project ([front](https://gitlab.com/web_bde_polytech_grenoble/front_web_bde) and [API](https://gitlab.com/web_bde_polytech_grenoble/api-bde) at least) (or clone) and run `npm install` to install all local module of your projects. You can work now or test the existing project [here : Running project : production](#running-project-production)

### Running project : production 

Run `npm start` to start your API.

Run `ng serve` and you can now access to your web page at http://localhost:4200/

### Running project : recette

Run `ng build` to product static files used at the end of the project. Then run `node server.js`.
Static files is generate in `dist/YOU_PROJECT` folder.

# Front

[Source code](https://gitlab.com/web_bde_polytech_grenoble/front_web_bde)

## Host

If you want to use a simple and free host you can use Heroku. But we use o2switch.

### o2switch

Nothing for the moment.

### Heroku

To use this Heroku host, you need to download HerokuCLI.

After that add file `server.js` on the root project with content :

    const express = require('express');
    const path = require('path');
    const app = express();

    // Serve static files....
    app.use(express.static(__dirname + '/dist/grebdepolytechweb'));

    // Send all requests to index.html
    app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/grebdepolytechweb/index.html'));
    });

    // default Heroku PORT
    app.listen(process.env.PORT || 3000);

Add in `package.json` on `script` part : 

    "start": "node server.js",
    "postinstall": "ng build --aot --prod"

##### Start using Heroku

Start to login to your Heroku account : `heroku login`.

Add heroku git repo : `heroku git:remote -a bdepolytechgrenoble`.

##### Push project

Start to login to your Heroku account : `heroku login`.

In first, be sur that the project work and clean. Then put `git push heroku --force`.

# Back

The web site need a back application.

You will need a [Firebase database](https://firebase.google.com) and the python application [here](https://gitlab.com/web_bde_polytech_grenoble/ade_scrapper).

#### Firebase

Go to Firebase web site, connect to your google account and add new Data Base. Create new project and in this project add web application. After that they give you a SDK Configuration.

Get back Firebase Configuration 

    firebaseConfig = {
        apiKey: "xxx",
        authDomain: "xxx",
        databaseURL: "xxx",
        projectId: "xxx",
        storageBucket: "xxx",
        messagingSenderId: "xxx",
        appId: "xxx"
    };

and copy/paste this on `src/environments/environment.prod.ts` and `src/environments/environment.ts` in `environment.firebase` variable.

**Warning** don't push on gitlab repo your ID connection to Firebase.

#### Python app

You need to add environment variable : 

... not finish ...

# PWA

### Open APP : Deep link

Allow, with smartphone, to open the native application instead of web page.

Exemple patern for some application : https://help.emplify.com/hc/en-us/articles/229307868-How-do-I-link-to-another-app-.

To open Facebook we need an ID that you can get here : https://www.duplichecker.com/find-facebook-id.php.

# Error

#### Chokidar : ENOSP

This means Chokidar ran out of file handles and you'll need to increase their count by executing the following command in Terminal. [More details](https://www.npmjs.com/package/chokidar#install-troubleshooting)