# Grebdepolytechweb

Project by BDE Polytech Grenoble

## Angular 
[Angular](./ANGULAR.md)

## Project Specification 
[Specification](./SPECIFICATION.md)

## Project Convention
[Convention](./CONVENTIONS.md)

## Other Help

How start with the project ?, etc...

[Help](./HELP.md)

---
    AUTHOR: Dorian Baret, Polytech'Grenoble, Univ. Grenoble Alpes 
    DATE: Août 2020